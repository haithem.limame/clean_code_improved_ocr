package fr.esgi.ocr;

import fr.esgi.kernel.exceptions.InvalidTagException;
import fr.esgi.ocr.exposition.CliLauncherArgsTag;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public final class CliLauncherArgsTagTest {

    @Test
    void of_should_return_the_correct_tag() {
        Assertions.assertEquals(CliLauncherArgsTag.DIRECTORY, CliLauncherArgsTag.of("-di"));
        Assertions.assertEquals(CliLauncherArgsTag.DIRECTORY, CliLauncherArgsTag.of("--directory"));
    }

    @Test
    void of_should_return_an_error() {
        Assertions.assertThrows(InvalidTagException.class, () -> CliLauncherArgsTag.of("FAKE"));
        Assertions.assertThrows(InvalidTagException.class, () -> CliLauncherArgsTag.of("-d"));
        Assertions.assertThrows(InvalidTagException.class, () -> CliLauncherArgsTag.of("--d"));
    }
}
