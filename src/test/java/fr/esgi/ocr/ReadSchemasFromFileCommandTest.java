package fr.esgi.ocr;

import fr.esgi.ocr.application.ReadSchemasFromFileCommand;
import fr.esgi.ocr.application.ReadSchemasFromFileCommandHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.util.List;

public class ReadSchemasFromFileCommandTest {
    private ReadSchemasFromFileCommand command;
    private ReadSchemasFromFileCommandHandler extractSchemasFromFile;

    @BeforeEach
    void setUp() {
        this.extractSchemasFromFile = new ReadSchemasFromFileCommandHandler();
        URL url = this.getClass().getResource("/ExtractSchemasFromFileCommandTest.esgi");
        assert url != null;
        this.command = new ReadSchemasFromFileCommand(url.getPath());
    }

    @Test
    //TODO
    public void ExtractSchemasFromFile() {
        List<String> schemas = extractSchemasFromFile.handle(command);
        String schema = "    _  _     _  _  _  _  _ \n  | _| _||_||_ |_   ||_||_|\n  ||_  _|  | _||_|  ||_| _|\n                           \n";
        Assertions.assertEquals(schemas.size(), 15);
        Assertions.assertEquals(schemas.get(0), schema);
        schemas.forEach(s -> Assertions.assertEquals(s, schema));
    }
}
