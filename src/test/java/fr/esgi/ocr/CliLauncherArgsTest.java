package fr.esgi.ocr;

import fr.esgi.ocr.application.*;
import fr.esgi.ocr.exposition.CliLauncherArgs;
import fr.esgi.ocr.exposition.CliLauncherArgsTag;
import fr.esgi.ocr.exposition.CliLauncherRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public final class CliLauncherArgsTest {

    CliLauncherRequest cliLauncherRequest;
    CliLauncherArgs cli;

    @BeforeEach
    void setUp() {
        OCRCommandHandler ocrCommandHandler = new OCRCommandHandler(new ReadDirectoryFilesQueryHandler(), new RetrieveCodeValidationQueryHandler(), new ReadSchemasFromFileCommandHandler(), new ParseSchemasToCodesCommandHandler());
        this.cliLauncherRequest = new CliLauncherRequest();
        this.cli = new CliLauncherArgs(ocrCommandHandler);
    }

    @Test
    void request_to_map_should_works_correctly() {
        this.cliLauncherRequest.setArgs("--file ./file -de ./detached".split(" "));
        Map<CliLauncherArgsTag, String> expected = new HashMap<>();
        expected.put(CliLauncherArgsTag.FILE, "./file");
        expected.put(CliLauncherArgsTag.DETACHED, "./detached");

        Map<CliLauncherArgsTag, String> actual = this.cli.requestToMap(this.cliLauncherRequest);
        Assertions.assertEquals(expected, actual);
    }
}
