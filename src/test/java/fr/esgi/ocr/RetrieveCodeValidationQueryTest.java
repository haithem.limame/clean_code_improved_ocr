package fr.esgi.ocr;

import fr.esgi.ocr.application.*;
import fr.esgi.ocr.domain.Code;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public final class RetrieveCodeValidationQueryTest {
    private ParseSchemasToCodesCommand parseSchemasToCodesCommand;
    private ParseSchemasToCodesCommandHandler parseSchemasToCodesCommandHandler;
    private RetrieveCodeValidationQueryHandler retrieveCodeValidationQueryHandler;
    private RetrieveCodeValidationQuery retrieveCodeValidationQuery;
    private String schema;
    private String errSchema;
    private String illSchema;

    @BeforeEach
    void setUp() {
        this.parseSchemasToCodesCommand = new ParseSchemasToCodesCommand();
        this.parseSchemasToCodesCommandHandler =new ParseSchemasToCodesCommandHandler();
        this.retrieveCodeValidationQueryHandler = new RetrieveCodeValidationQueryHandler();
        this.retrieveCodeValidationQuery = new RetrieveCodeValidationQuery();
        schema =        "    _  _  _  _  _  _  _  _ \n" +
                        "|_||_   ||_ | ||_|| || || |\n" +
                        "  | _|  | _||_||_||_||_||_|\n" +
                        "                           \n";
        errSchema =     "    _  _     _  _  _  _    \n" +
                        "  | _| _||_||_ |_   ||_|  |\n" +
                        "  ||_  _|  | _||_|  ||_|  |\n" +
                        "                           \n";

        illSchema =     "    _  _     _  _  _  _    \n" +
                        "  | _| _||_||_ |_    |_|  |\n" +
                        "  ||_  _|  | _||_|  ||_|  |\n" +
                        "                           \n";
    }

    @Test
    void is_checksum_valid_should_return_true() {
        parseSchemasToCodesCommand.setSchemas(List.of(this.schema));
        Code code = parseSchemasToCodesCommandHandler.handle(parseSchemasToCodesCommand).get(0);
        retrieveCodeValidationQuery.setCode(code);
        Assertions.assertTrue(retrieveCodeValidationQueryHandler.handle(retrieveCodeValidationQuery));
    }

    @Test
    void is_checksum_valid_should_return_false() {
        parseSchemasToCodesCommand.setSchemas(List.of(this.errSchema, this.illSchema));
        Code codeErr = parseSchemasToCodesCommandHandler.handle(parseSchemasToCodesCommand).get(0);
        Code codeIll = parseSchemasToCodesCommandHandler.handle(parseSchemasToCodesCommand).get(1);
        retrieveCodeValidationQuery.setCode(codeErr);
        Assertions.assertFalse(retrieveCodeValidationQueryHandler.handle(retrieveCodeValidationQuery));
        retrieveCodeValidationQuery.setCode(codeIll);
        Assertions.assertFalse(retrieveCodeValidationQueryHandler.handle(retrieveCodeValidationQuery));
    }

}
