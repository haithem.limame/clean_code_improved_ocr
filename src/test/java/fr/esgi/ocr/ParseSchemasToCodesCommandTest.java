package fr.esgi.ocr;

import fr.esgi.ocr.application.ReadSchemasFromFileCommand;
import fr.esgi.ocr.application.ReadSchemasFromFileCommandHandler;
import fr.esgi.ocr.application.ParseSchemasToCodesCommand;
import fr.esgi.ocr.application.ParseSchemasToCodesCommandHandler;
import fr.esgi.ocr.domain.Code;
import fr.esgi.ocr.domain.Number;
import fr.esgi.ocr.infrastructure.InMemoryCodes;
import fr.esgi.ocr.infrastructure.InMemoryNumbers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.util.List;

public final class ParseSchemasToCodesCommandTest {

    InMemoryCodes codes;
    ParseSchemasToCodesCommand command;
    ParseSchemasToCodesCommandHandler handler;
    List<Code> expectedCodes;

    @BeforeEach
    void setUp() {
        this.codes = InMemoryCodes.getInstance();
        URL url = this.getClass().getResource("/MapSchemasToCodesCommandTest.esgi");
        ReadSchemasFromFileCommand readSchemasFromFileCommand = new ReadSchemasFromFileCommand(url.getPath());
        ReadSchemasFromFileCommandHandler readSchemasFromFileCommandHandler = new ReadSchemasFromFileCommandHandler();
        List<String> schemas = readSchemasFromFileCommandHandler.handle(readSchemasFromFileCommand);

        List<Number> numbersOfSchema1 = List.of(
                Number.of(InMemoryNumbers.valueOfLabel("1").getSchema(), 9),
                Number.of(InMemoryNumbers.valueOfLabel("2").getSchema(), 8),
                Number.of(InMemoryNumbers.valueOfLabel("3").getSchema(), 7),
                Number.of(InMemoryNumbers.valueOfLabel("4").getSchema(), 6),
                Number.of(InMemoryNumbers.valueOfLabel("5").getSchema(), 5),
                Number.of(InMemoryNumbers.valueOfLabel("6").getSchema(), 4),
                Number.of(InMemoryNumbers.valueOfLabel("7").getSchema(), 3),
                Number.of(InMemoryNumbers.valueOfLabel("8").getSchema(), 2),
                Number.of(InMemoryNumbers.valueOfLabel("9").getSchema(), 1)
        );
        List<Number> numbersOfSchema2 = List.of(
                Number.of(InMemoryNumbers.valueOfLabel("5").getSchema(), 9),
                Number.of(InMemoryNumbers.valueOfLabel("2").getSchema(), 8),
                Number.of(InMemoryNumbers.valueOfLabel("3").getSchema(), 7),
                Number.of(InMemoryNumbers.valueOfLabel("5").getSchema(), 6),
                Number.of(InMemoryNumbers.valueOfLabel("5").getSchema(), 5),
                Number.of(InMemoryNumbers.valueOfLabel("5").getSchema(), 4),
                Number.of(InMemoryNumbers.valueOfLabel("5").getSchema(), 3),
                Number.of(InMemoryNumbers.valueOfLabel("8").getSchema(), 2),
                Number.of(InMemoryNumbers.valueOfLabel("9").getSchema(), 1)
        );
        this.expectedCodes = List.of(
                Code.of(this.codes.nextIdentity(), numbersOfSchema1),
                Code.of(this.codes.nextIdentity(), numbersOfSchema2));

        this.command = new ParseSchemasToCodesCommand(schemas);
        this.handler = new ParseSchemasToCodesCommandHandler();
    }

    @Test
    void parse_correctly_schemas_to_codes() {
        List<Code> codes = this.handler.handle(this.command);
        Assertions.assertEquals(this.expectedCodes.get(0), codes.get(0));
        Assertions.assertEquals(this.expectedCodes.get(1), codes.get(1));
    }

    @Test
    void parse_correctly_illegible_schema() {
        List<Code> codes = this.handler.handle(this.command);
        Assertions.assertFalse(codes.get(1).isIllegible());
        Assertions.assertTrue(codes.get(2).isIllegible());
    }
}
