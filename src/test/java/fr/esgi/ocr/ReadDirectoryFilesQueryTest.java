package fr.esgi.ocr;

import fr.esgi.ocr.application.*;
import fr.esgi.ocr.domain.Code;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.util.List;
import java.util.Objects;

public final class ReadDirectoryFilesQueryTest {
    private ReadDirectoryFilesQuery directoryFilesQuery;
    private ReadDirectoryFilesQueryHandler readDirectoryFilesQueryHandler;

    @BeforeEach
    void setUp() {
        URL url = this.getClass().getResource("/readDirectoryFilesQueryTest/");
        assert url != null;
        this.directoryFilesQuery = new ReadDirectoryFilesQuery(url.getPath());
        this.readDirectoryFilesQueryHandler = new ReadDirectoryFilesQueryHandler();
    }

    @Test
    void read_directory_files() {
        List<String> filesPath = readDirectoryFilesQueryHandler.handle(directoryFilesQuery);
        List<String> expectedFilesPath=List.of("A","B");
        for (int i = 0; i < expectedFilesPath.size(); i++) {
            Assertions.assertEquals(filesPath.indexOf(expectedFilesPath.get(i)),-1);
        }
    }
}
