package fr.esgi.kernel.exceptions;


public class InvalidSchemaException extends RuntimeException {

    public InvalidSchemaException() {
        super(CodeMessage.INVALID_SCHEMA);
    }

    public InvalidSchemaException(Throwable cause) {
        super(CodeMessage.INVALID_SCHEMA, cause);
    }
}
