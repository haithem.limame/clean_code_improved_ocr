package fr.esgi.kernel.exceptions;


import fr.esgi.kernel.Constants;

public class InvalidTagException extends RuntimeException {

    public InvalidTagException(String message) {
        super(CodeMessage.INVALID_TAG + Constants.SPACE + message);
    }

    public InvalidTagException(Throwable cause) {
        super(CodeMessage.INVALID_TAG, cause);
    }
}
