package fr.esgi.kernel.exceptions;

public class CodeMessage {

    public static final String INVALID_SCHEMA = "FUNC.3001";
    public static final String FILE_NOT_FOUND = "FUNC.3002";
    public static final String INVALID_TAG = "FUNC.3003";

}
