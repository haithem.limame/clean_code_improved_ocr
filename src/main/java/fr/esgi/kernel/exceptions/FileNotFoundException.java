package fr.esgi.kernel.exceptions;


public class FileNotFoundException extends RuntimeException {

    public FileNotFoundException() {
        super(CodeMessage.FILE_NOT_FOUND);
    }

    public FileNotFoundException(Throwable cause) {
        super(CodeMessage.FILE_NOT_FOUND, cause);
    }
}
