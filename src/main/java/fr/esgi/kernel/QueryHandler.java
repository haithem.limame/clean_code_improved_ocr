package fr.esgi.kernel;

import java.io.IOException;
import java.net.URISyntaxException;

@FunctionalInterface
public interface QueryHandler<Q extends Query, R> {
    R handle(Q query) throws IOException, URISyntaxException;
}
