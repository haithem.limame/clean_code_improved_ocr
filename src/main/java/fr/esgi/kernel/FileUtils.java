package fr.esgi.kernel;

public final class FileUtils {

    public static boolean isElementOutOfIndex(int limit, int index) {
        return limit <= index;
    }

    public static String help() {
        return "\n\n" +
                "\n/help .. how to use our program" +
                "\n--------------------------------------------------------" +
                "\n\t1. There is 2 ways to use it" +
                "\n\t\t - with arguments" +
                "\n\t\t - with the interactive interface" +
                "\n--------------------------------------------------------" +
                "\n\t2. ARGUMENTS" +
                "\n\t\tschema : %tagInputFile %path %tagOutputFile %path" +
                "\n\t\texample : java -jar myJar.jar -f ./test.txt --regrouped ./" +
                "\n\n\t\toptions :" +
                "\n\t\t\t> %tagInputFile : " +
                "\n\t\t\t\t-f, --file (give only 1 file)" +
                "\n\t\t\t\t-di, --directory (give only a directory of files)" +
                "\n\t\t\t> %tagOutputFile : " +
                "\n\t\t\t\t-r, --regrouped (return only 1 file)" +
                "\n\t\t\t\t-de, --detached (return 3 files : authorized, errored, et unknown)" +
                "\n--------------------------------------------------------" +
                "\n\t3. INTERACTIVE" +
                "\nTODO" +
                "\n\n";
    }
}
