package fr.esgi.kernel;

public class Constants {
    public final static String EOL = "\n";
    public final static String EMPTY = "";
    public final static String ILL = "ILL";
    public final static String ERR = "ERR";
    public final static String SPACE = " ";
    public final static String SLASH = "/";
}
