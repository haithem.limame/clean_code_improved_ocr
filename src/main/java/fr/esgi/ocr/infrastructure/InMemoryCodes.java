package fr.esgi.ocr.infrastructure;

import fr.esgi.ocr.domain.Code;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

public final class InMemoryCodes {

    private final Logger log = Logger.getLogger(InMemoryCodes.class.getName());
    private final AtomicInteger count;
    private final Map<Integer, Code> data;
    private static final InMemoryCodes INSTANCE = new InMemoryCodes();

    private InMemoryCodes() {
        this.count = new AtomicInteger();
        this.data = new ConcurrentHashMap<>();
    }

    public static InMemoryCodes getInstance() {
        return INSTANCE;
    }

    public List<Code> findAll() {
        log.info(String.format("[CODES] : findAll() -> %d codes", this.data.size()));
        return new ArrayList<>(this.data.values());
    }

    public int nextIdentity() {
//        log.info("[CODES] : nextIdentity()");
        return this.count.getAndIncrement();
    }

    public void add(Code code) {
        log.info("[CODES] : add() -> " + code.getValue());
        this.data.put(code.getId(), code);
    }

    public Code findLast() {
        return this.data.get(this.count.get());
    }

    public Code findById(int id) {
        return this.data.get(id);
    }
}
