package fr.esgi.ocr.infrastructure;

import java.util.HashMap;
import java.util.Map;

public enum InMemoryNumbers {
    ZERO  ("0", " _ \n| |\n|_|\n   \n"),
    ONE   ("1", "   \n  |\n  |\n   \n"),
    TWO   ("2", " _ \n _|\n|_ \n   \n"),
    THREE ("3", " _ \n _|\n _|\n   \n"),
    FOUR  ("4", "   \n|_|\n  |\n   \n"),
    FIVE  ("5", " _ \n|_ \n _|\n   \n"),
    SIX   ("6", " _ \n|_ \n|_|\n   \n"),
    SEVEN ("7", " _ \n  |\n  |\n   \n"),
    EIGHT ("8", " _ \n|_|\n|_|\n   \n"),
    NINE  ("9", " _ \n|_|\n _|\n   \n"),
    ERR  ("?", "?");

    private static final Map<String, InMemoryNumbers> BY_LABEL = new HashMap<>();
    private static final Map<String, InMemoryNumbers> BY_SCHEMA = new HashMap<>();
    static {
        for (InMemoryNumbers e : values()) {
            BY_LABEL.put(e.label, e);
            BY_SCHEMA.put(e.schema, e);
        }
    }
    private final String label;
    private final String schema;

    private InMemoryNumbers(String label, String schema) {
        this.label = label;
        this.schema = schema;
    }

    public static InMemoryNumbers valueOfLabel(String label) {
        return BY_LABEL.get(label)!=null ? BY_LABEL.get(label): ERR ;
    }

    public static InMemoryNumbers valueOfSchema(String schema) {
        return BY_SCHEMA.get(schema)!=null ? BY_SCHEMA.get(schema): ERR;
    }

    public String getLabel() {
        return label;
    }

    public String getSchema() {
        return schema;
    }
}
