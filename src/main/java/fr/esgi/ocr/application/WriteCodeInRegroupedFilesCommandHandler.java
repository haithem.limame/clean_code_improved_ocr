package fr.esgi.ocr.application;

import fr.esgi.kernel.Constants;
import fr.esgi.kernel.exceptions.FileNotFoundException;
import fr.esgi.ocr.domain.Code;
import fr.esgi.ocr.infrastructure.InMemoryCodes;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Logger;


public final class WriteCodeInRegroupedFilesCommandHandler implements WriteCodeInFilesCommandHandler<WriteCodeInFilesCommand,Void> {

    private final Logger log = Logger.getLogger(WriteCodeInRegroupedFilesCommandHandler.class.getName());
    private final RetrieveCodeValidationQueryHandler codeValidator;
    private final InMemoryCodes codes = InMemoryCodes.getInstance();

    public WriteCodeInRegroupedFilesCommandHandler(RetrieveCodeValidationQueryHandler codeValidator) {
        this.codeValidator = codeValidator;
    }

    @Override
    public Void handle(WriteCodeInFilesCommand command) {
        try {
            Date today = new Date();
            String fileName = "ocr_" + today.getTime() + ".esgi";
            String path = command.getPath().concat(fileName);

            BufferedWriter writer = new BufferedWriter(new FileWriter(path));
            for (Code code : this.codes.findAll()) {
                StringBuilder content = new StringBuilder();
                RetrieveCodeValidationQuery retrieveCodeValidationQuery = new RetrieveCodeValidationQuery(code);
                if (code.isIllegible()) {
                    content.append(code.getValue()).append(Constants.SPACE).append(Constants.ILL).append(Constants.EOL);
                } else if (codeValidator.handle(retrieveCodeValidationQuery)) {
                    content.append(code.getValue()).append(Constants.EOL);
                } else {
                    content.append(code.getValue()).append(Constants.SPACE).append(Constants.ERR).append(Constants.EOL);
                }
                writer.write(content.toString());
            }
            writer.close();
            this.log.info("[CREATED_FILE] : " + path);
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
        return null;
    }
}
