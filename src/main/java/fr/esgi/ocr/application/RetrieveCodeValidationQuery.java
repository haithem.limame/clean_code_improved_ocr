package fr.esgi.ocr.application;

import fr.esgi.kernel.Query;
import fr.esgi.ocr.domain.Code;

public final class RetrieveCodeValidationQuery implements Query {
    private Code code;

    public RetrieveCodeValidationQuery() {
        // Empty constructor
    }

    public RetrieveCodeValidationQuery(Code code) {
        this.code = code;
    }

    public Code getCode() {
        return code;
    }

    public void setCode(Code code) {
        this.code = code;
    }
}
