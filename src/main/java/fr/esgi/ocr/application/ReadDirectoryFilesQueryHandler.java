package fr.esgi.ocr.application;

import fr.esgi.kernel.QueryHandler;
import fr.esgi.kernel.exceptions.FileNotFoundException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class ReadDirectoryFilesQueryHandler implements QueryHandler<ReadDirectoryFilesQuery, List<String>> {
    @Override
    public List<String> handle(ReadDirectoryFilesQuery query) {
        File folder = new File(query.getPath());
        return Arrays.stream(folder.listFiles())
                .map(File::getPath)
                .collect(Collectors.toList());
        /*try (Stream<Path> paths = Files.walk(Paths.get(query.getPath()))) {
            return paths
                    .filter(Files::isRegularFile)
                    .map(path -> path.toAbsolutePath().toString())
                    .collect(Collectors.toList());
        } catch (IOException exception) {
            throw new FileNotFoundException();
        }*/
    }
}
