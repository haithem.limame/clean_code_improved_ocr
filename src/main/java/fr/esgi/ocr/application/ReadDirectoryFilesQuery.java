package fr.esgi.ocr.application;

import fr.esgi.kernel.Query;

public final class ReadDirectoryFilesQuery implements Query {
    private String path;

    public ReadDirectoryFilesQuery(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
