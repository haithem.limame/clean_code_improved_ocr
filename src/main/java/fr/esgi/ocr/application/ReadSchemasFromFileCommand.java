package fr.esgi.ocr.application;

import fr.esgi.kernel.Command;

public final class ReadSchemasFromFileCommand implements Command {
    private String path;

    public ReadSchemasFromFileCommand() {
        // Empty constructor
    }

    public ReadSchemasFromFileCommand(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
