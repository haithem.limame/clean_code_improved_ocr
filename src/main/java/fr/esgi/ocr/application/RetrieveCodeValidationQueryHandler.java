package fr.esgi.ocr.application;

import fr.esgi.kernel.QueryHandler;
import fr.esgi.ocr.domain.Code;

public final class RetrieveCodeValidationQueryHandler implements QueryHandler<RetrieveCodeValidationQuery, Boolean> {
    
    @Override
    public Boolean handle(RetrieveCodeValidationQuery query) {
        Code code = query.getCode();
        if (!code.isIllegible()) {
            int sum = code.getNumbers().stream()
                    .mapToInt(number -> Integer.parseInt(number.getValue().getLabel()) * number.getPosition())
                    .sum();

            return sum % 11 == 0;
        }
        return false;
    }
}
