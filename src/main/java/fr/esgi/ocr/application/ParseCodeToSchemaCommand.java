package fr.esgi.ocr.application;

import fr.esgi.kernel.Command;
import fr.esgi.ocr.domain.Code;

public final class ParseCodeToSchemaCommand implements Command {
    private Code code;

    public ParseCodeToSchemaCommand() {
        // Empty constructor
    }

    public ParseCodeToSchemaCommand(Code code) {
        this.code = code;
    }

    public Code getCode() {
        return code;
    }

    public void setCode(Code code) {
        this.code = code;
    }
}
