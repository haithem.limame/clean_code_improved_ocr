package fr.esgi.ocr.application;

import fr.esgi.kernel.Command;

import java.util.List;

public final class ParseSchemasToCodesCommand implements Command {
    private List<String> schemas;

    public ParseSchemasToCodesCommand() {
        // Empty constructor
    }

    public ParseSchemasToCodesCommand(List<String> schemas) {
        this.schemas = schemas;
    }

    public List<String> getSchemas() {
        return schemas;
    }

    public void setSchemas(List<String> schemas) {
        this.schemas = schemas;
    }
}
