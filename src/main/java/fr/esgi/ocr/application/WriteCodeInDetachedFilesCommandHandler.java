package fr.esgi.ocr.application;

import fr.esgi.kernel.Constants;
import fr.esgi.kernel.exceptions.FileNotFoundException;
import fr.esgi.ocr.domain.Code;
import fr.esgi.ocr.infrastructure.InMemoryCodes;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Logger;


public final class WriteCodeInDetachedFilesCommandHandler implements WriteCodeInFilesCommandHandler<WriteCodeInFilesCommand, Void> {

    private final Logger log = Logger.getLogger(WriteCodeInDetachedFilesCommandHandler.class.getName());
    private final RetrieveCodeValidationQueryHandler codeValidator;
    private final InMemoryCodes codes = InMemoryCodes.getInstance();

    public WriteCodeInDetachedFilesCommandHandler(RetrieveCodeValidationQueryHandler codeValidator) {
        this.codeValidator = codeValidator;
    }

    @Override
    public Void handle(WriteCodeInFilesCommand command) {
        try {
            Date today = new Date();
            String fileName = "ocr_" + today.getTime() + "_";
            String fileNameAuthorized = command.getPath().concat(fileName).concat("Authorized.esgi");
            String fileNameUnknown = command.getPath().concat(fileName).concat("Unknown.esgi");
            String fileNameErrored = command.getPath().concat(fileName).concat("Errored.esgi");

            BufferedWriter writer = new BufferedWriter(new FileWriter(fileNameAuthorized));
            BufferedWriter writerILL = new BufferedWriter(new FileWriter(fileNameUnknown));
            BufferedWriter writerERR = new BufferedWriter(new FileWriter(fileNameErrored));

            for (Code code : this.codes.findAll()) {
                StringBuilder content = new StringBuilder();
                StringBuilder contentILL = new StringBuilder();
                StringBuilder contentERR = new StringBuilder();
                RetrieveCodeValidationQuery retrieveCodeValidationQuery = new RetrieveCodeValidationQuery(code);

                if (code.isIllegible()) {
                    contentILL.append(code.getValue()).append(Constants.SPACE).append(Constants.ILL).append(Constants.EOL);
                } else if (Boolean.TRUE.equals(codeValidator.handle(retrieveCodeValidationQuery))) {
                    content.append(code.getValue()).append(Constants.EOL);
                } else {
                    contentERR.append(code.getValue()).append(Constants.SPACE).append(Constants.ERR).append(Constants.EOL);
                }

                writer.write(content.toString());
                writerILL.write(contentILL.toString());
                writerERR.write(contentERR.toString());
            }

            writer.close();
            writerILL.close();
            writerERR.close();
            this.log.info("[CREATED_FILE] : " + fileNameAuthorized);
            this.log.info("[CREATED_FILE] : " + fileNameUnknown);
            this.log.info("[CREATED_FILE] : " + fileNameErrored);
        } catch (IOException e) {
            throw new FileNotFoundException();
        }

        return null;
    }
}
