package fr.esgi.ocr.application;

import fr.esgi.kernel.Command;

public final class OCRCommand implements Command {

    private final boolean isDirectory;
    private final String inputPath;
    private final boolean isRegrouped;
    private final String outputPath;

    public OCRCommand(boolean isDirectory, String inputPath, boolean isRegrouped, String outputPath) {
        this.isDirectory = isDirectory;
        this.inputPath = inputPath;
        this.isRegrouped = isRegrouped;
        this.outputPath = outputPath;
    }

    public String getInputPath() {
        return inputPath;
    }

    public boolean isDirectory() {
        return isDirectory;
    }

    public boolean isRegrouped() {
        return isRegrouped;
    }

    public String getOutputPath() {
        return outputPath;
    }
}
