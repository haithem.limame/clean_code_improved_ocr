package fr.esgi.ocr.application;

import fr.esgi.kernel.CommandHandler;
import fr.esgi.kernel.Constants;
import fr.esgi.ocr.domain.Number;

public final class ParseCodeToSchemaCommandHandler implements CommandHandler<ParseCodeToSchemaCommand,String> {

    @Override
    public String handle(ParseCodeToSchemaCommand command) {
        StringBuilder result = new StringBuilder();
        for (int row = 0; row < command.getCode().row(); row++) {
            StringBuilder rowCode = new StringBuilder();
            for (Number number : command.getCode().getNumbers()) {
                int start = Number.COL * row;
                int end = start + Number.COL;
                rowCode.append(number.getSchema().replace(Constants.EOL, Constants.SPACE), start, end);
            }
            result.append(rowCode).append(Constants.EOL);
        }
        return result.toString();
    }
}
