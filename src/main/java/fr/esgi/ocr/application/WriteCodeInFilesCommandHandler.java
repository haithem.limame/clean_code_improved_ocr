package fr.esgi.ocr.application;

import fr.esgi.kernel.Command;
import fr.esgi.kernel.CommandHandler;

public interface WriteCodeInFilesCommandHandler<WriteCodeInFilesCommand extends Command,Void> extends CommandHandler<WriteCodeInFilesCommand,Void> {
}
