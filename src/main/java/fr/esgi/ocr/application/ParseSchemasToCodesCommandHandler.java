package fr.esgi.ocr.application;

import fr.esgi.kernel.CommandHandler;
import fr.esgi.kernel.Constants;
import fr.esgi.ocr.domain.Code;
import fr.esgi.ocr.domain.Number;
import fr.esgi.ocr.infrastructure.InMemoryCodes;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class ParseSchemasToCodesCommandHandler implements CommandHandler<ParseSchemasToCodesCommand, List<Code>> {

    InMemoryCodes codes = InMemoryCodes.getInstance();

    @Override
    public List<Code> handle(ParseSchemasToCodesCommand command) {
        return command.getSchemas()
                .stream()
                .map(this::mapSchema)
                .collect(Collectors.toList());
    }

    private Code mapSchema(String schema) {
        Code code = Code.withoutNumbers(codes.nextIdentity());
        List<String> raw = Arrays.asList(schema.split(Constants.EOL));
        int lineSize = raw.get(0).length();
        for (int x = 0; x < lineSize; x += Number.COL) {
            StringBuilder numberSchema = new StringBuilder();
            for (String line : raw) {
                numberSchema.append(line, x, x + Number.COL).append(Constants.EOL);
            }
            code.addNumber(Number.of(numberSchema.toString(), 9 - x / Number.COL));
        }

        this.codes.add(code);
        return code;
    }

}
