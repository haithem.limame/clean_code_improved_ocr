package fr.esgi.ocr.application;

import fr.esgi.kernel.CommandHandler;
import fr.esgi.kernel.Constants;
import fr.esgi.kernel.exceptions.FileNotFoundException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public final class ReadSchemasFromFileCommandHandler implements CommandHandler<ReadSchemasFromFileCommand, List<String>> {

    private final Logger log = Logger.getLogger(this.getClass().getName());

    @Override
    public List<String> handle(ReadSchemasFromFileCommand command) {
        log.info("[READ_FILE] : " + command.getPath());
        List<String> result = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(command.getPath()))) {
            var currentLine = Constants.EMPTY;
            var currentCodeSchema = new StringBuilder();
            var index = 1;
            while ((currentLine = reader.readLine()) != null) {
                currentCodeSchema.append(currentLine).append(Constants.EOL);
                if (index % 4 == 0) {
                    result.add(currentCodeSchema.toString());
                    currentCodeSchema = new StringBuilder();
                }
                index++;
            }
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
        return result;
    }
}
