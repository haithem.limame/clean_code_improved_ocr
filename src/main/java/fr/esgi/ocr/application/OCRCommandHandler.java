package fr.esgi.ocr.application;

import fr.esgi.kernel.CommandHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OCRCommandHandler implements CommandHandler<OCRCommand, Void> {

    private final ReadDirectoryFilesQueryHandler readDirectoryFilesQueryHandler;
    private final RetrieveCodeValidationQueryHandler retrieveCodeValidationQueryHandler;
    private final ReadSchemasFromFileCommandHandler readSchemasFromFileCommandHandler;
    private final ParseSchemasToCodesCommandHandler parseSchemasToCodesCommandHandler;
    private WriteCodeInFilesCommandHandler writeCodeInFilesCommandHandler;

    public OCRCommandHandler(ReadDirectoryFilesQueryHandler readDirectoryFilesQueryHandler, RetrieveCodeValidationQueryHandler retrieveCodeValidationQueryHandler, ReadSchemasFromFileCommandHandler readSchemasFromFileCommandHandler, ParseSchemasToCodesCommandHandler parseSchemasToCodesCommandHandler) {
        this.readDirectoryFilesQueryHandler = readDirectoryFilesQueryHandler;
        this.retrieveCodeValidationQueryHandler = retrieveCodeValidationQueryHandler;
        this.readSchemasFromFileCommandHandler = readSchemasFromFileCommandHandler;
        this.parseSchemasToCodesCommandHandler = parseSchemasToCodesCommandHandler;
    }

    @Override
    public Void handle(OCRCommand ocrCommand) {
        List<String> pathFiles = getFilesPathFromDirectory(ocrCommand);
        List<String> schemas = getSchemasFromFiles(pathFiles);
        parseSchemasToCodesCommandHandler.handle(new ParseSchemasToCodesCommand(schemas));
        writeOutput(ocrCommand);
        return null;
    }

    public List<String> getFilesPathFromDirectory(OCRCommand ocrCommand) {
        List<String> result = new ArrayList<>();

        if (ocrCommand.isDirectory()) {
            ReadDirectoryFilesQuery readDirectoryFilesQuery = new ReadDirectoryFilesQuery(ocrCommand.getInputPath());
            result = readDirectoryFilesQueryHandler.handle(readDirectoryFilesQuery);
        } else {
            result.add(ocrCommand.getInputPath());
        }

        return result;
    }

    public List<String> getSchemasFromFiles(List<String> pathFiles) {
        List<String> result = new ArrayList<>();

        for (String pathFile : pathFiles) {
            List<String> currentSchemas = readSchemasFromFileCommandHandler.handle(new ReadSchemasFromFileCommand(pathFile));
            result = Stream.concat(result.stream(), currentSchemas.stream()).collect(Collectors.toList());
        }

        return result;
    }

    public void writeOutput(OCRCommand ocrCommand) {
        WriteCodeInFilesCommand writeCodeInFilesCommand = new WriteCodeInFilesCommand(ocrCommand.getOutputPath());

        if (ocrCommand.isRegrouped())
            writeCodeInFilesCommandHandler = new WriteCodeInRegroupedFilesCommandHandler(retrieveCodeValidationQueryHandler);
        else
            writeCodeInFilesCommandHandler = new WriteCodeInDetachedFilesCommandHandler(retrieveCodeValidationQueryHandler);

        writeCodeInFilesCommandHandler.handle(writeCodeInFilesCommand);
    }
}
