package fr.esgi.ocr.application;

import fr.esgi.kernel.Command;
import fr.esgi.ocr.domain.Code;

import java.util.List;

public final class WriteCodeInFilesCommand implements Command {
    private String path;

    public WriteCodeInFilesCommand(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
