package fr.esgi.ocr.exposition;

import java.io.IOException;

public interface CliLauncher {

    void run(CliLauncherRequest cliLauncherRequest) throws IOException;
}
