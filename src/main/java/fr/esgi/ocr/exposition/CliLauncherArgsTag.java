package fr.esgi.ocr.exposition;

import fr.esgi.kernel.exceptions.InvalidTagException;

public enum CliLauncherArgsTag {
    DIRECTORY("--directory", "-di"),
    FILE("--file", "-f"),
    REGROUPED("--regrouped", "-r"),
    DETACHED("--detached", "-de");

    private final String fullName;
    private final String shortName;

    CliLauncherArgsTag(String fullName, String shortName) {
        this.fullName = fullName;
        this.shortName = shortName;
    }

    public static CliLauncherArgsTag of(String value) {
        for (CliLauncherArgsTag it : CliLauncherArgsTag.values()) {
            if (it.getFullName().equals(value) || it.getShortName().equals(value))
                return it;
        }

        throw new InvalidTagException(String.format("ERROR : [%s] is not a correct TAG", value));
    }

    public static boolean isTag(String value) {
        for (CliLauncherArgsTag it : CliLauncherArgsTag.values()) {
            if (it.getFullName().equals(value) || it.getShortName().equals(value))
                return true;
        }

        return false;
    }

    public String getFullName() {
        return fullName;
    }

    public String getShortName() {
        return shortName;
    }
}
