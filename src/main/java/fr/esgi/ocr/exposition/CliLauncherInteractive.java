package fr.esgi.ocr.exposition;

import fr.esgi.ocr.application.*;

import java.util.Objects;
import java.util.Scanner;

public class CliLauncherInteractive implements CliLauncher{
    private final OCRCommandHandler ocrCommandHandler ;


    public CliLauncherInteractive(OCRCommandHandler ocrCommandHandler) {
        this.ocrCommandHandler = ocrCommandHandler;
    }

    @Override
    public void run(CliLauncherRequest cliLauncherRequest) {
        //create the Scanner
        Scanner terminalInput = new Scanner(System.in);
        boolean isDirectory;
        boolean isRegrouped;
        String inputPath;
        String outputPath;

        System.out.println("1. Do you want to process a directory (y)");
        isDirectory = Objects.equals(terminalInput.nextLine().toLowerCase(), "y");

        if (isDirectory) {
            System.out.println("2. Indicate the path of the directory");
        } else {
            System.out.println("2. Indicate the path of the file");
        }
        inputPath = terminalInput.nextLine();

        System.out.println("3. Do you want regrouped the files (y)");
        isRegrouped = Objects.equals(terminalInput.nextLine().toLowerCase(), "y");

        System.out.println("4. Indicate the path of the directory");
        outputPath = terminalInput.nextLine();

        System.out.println("\nOK .......");
        ocrCommandHandler.handle(new OCRCommand(isDirectory, inputPath, isRegrouped, outputPath));
    }
}