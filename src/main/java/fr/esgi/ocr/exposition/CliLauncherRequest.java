package fr.esgi.ocr.exposition;

public class CliLauncherRequest {
    private String[] args;

    public CliLauncherRequest() {
    }

    public CliLauncherRequest(String[] args) {
        this.args = args;
    }

    public String[] getArgs() {
        return args;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }
}
