package fr.esgi.ocr.exposition;


import fr.esgi.kernel.FileUtils;
import fr.esgi.ocr.application.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static fr.esgi.kernel.FileUtils.isElementOutOfIndex;

public class CliLauncherArgs implements CliLauncher {

    private Map<CliLauncherArgsTag, String> command;
    private final OCRCommandHandler ocrCommandHandler;

    public CliLauncherArgs(OCRCommandHandler ocrCommandHandler) {
        this.ocrCommandHandler = ocrCommandHandler;
    }

    @Override
    public void run(CliLauncherRequest cliLauncherRequest) {
        isValidRequest(cliLauncherRequest);
        this.command = requestToMap(cliLauncherRequest);
        this.command.forEach((tag, value) -> System.out.printf("[%s, %s]\n", tag, value));
        ocrCommandHandler.handle(new OCRCommand(isDirectory(), getInputPath(), isRegrouped(), getOutputPath()));
    }

    @Deprecated
    public void display(CliLauncherRequest cliLauncherRequest) {
        Arrays.stream(cliLauncherRequest.getArgs()).forEach(System.out::println);
    }

    public boolean isValidRequest(CliLauncherRequest cliLauncherRequest) {
        String[] args = cliLauncherRequest.getArgs();
        Arrays.stream(args).filter(it -> it.charAt(0) == '-').forEach(CliLauncherArgsTag::isTag);
        return true;
    }

    public boolean isValidPath(String path) {
        //TODO
        return true;
    }

    public Map<CliLauncherArgsTag, String> requestToMap(CliLauncherRequest cliLauncherRequest) {
        String[] args = cliLauncherRequest.getArgs();
        Map<CliLauncherArgsTag, String> result = new HashMap<>();

        for (int i = 0; i < args.length; i++) {
            String it = args[i];

            if (CliLauncherArgsTag.isTag(it)) {
                if (isElementOutOfIndex(args.length, i + 1))
                    throw new IllegalArgumentException(String.format("The element [%s] is alone", it));

                CliLauncherArgsTag tag = CliLauncherArgsTag.of(it);
                String value = args[i + 1];
                result.put(tag, value);
            }
        }

        return result;
    }

    public boolean isDirectory() {
        if (this.command.containsKey(CliLauncherArgsTag.FILE)) return false;
        else if (this.command.containsKey(CliLauncherArgsTag.DIRECTORY)) return true;
        else throw new IllegalArgumentException("Input TAG missing" + FileUtils.help());
    }

    public boolean isRegrouped() {
        if (this.command.containsKey(CliLauncherArgsTag.DETACHED)) return false;
        else if (this.command.containsKey(CliLauncherArgsTag.REGROUPED)) return true;
        else throw new IllegalArgumentException("Output TAG missing" + FileUtils.help());
    }

    public String getInputPath() {
        if (this.command.containsKey(CliLauncherArgsTag.FILE))
            return this.command.get(CliLauncherArgsTag.FILE);
        else return this.command.get(CliLauncherArgsTag.DIRECTORY);
    }

    public String getOutputPath() {
        if (this.command.containsKey(CliLauncherArgsTag.REGROUPED))
            return this.command.get(CliLauncherArgsTag.REGROUPED);
        else return this.command.get(CliLauncherArgsTag.DETACHED);
    }
}
