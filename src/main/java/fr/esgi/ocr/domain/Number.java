package fr.esgi.ocr.domain;

import fr.esgi.kernel.Constants;
import fr.esgi.kernel.ValueObject;
import fr.esgi.ocr.infrastructure.InMemoryNumbers;

import java.util.Objects;

public final class Number implements ValueObject {

    public static final int ROW = 4;
    public static final int COL = 3;
    private final InMemoryNumbers value;
    private final String schema;
    private final Integer position;

    private Number(String schema,Integer position) {
        if (!isValid(schema)) {
            System.err.println(schema);
           // throw new InvalidSchemaException();
        }
        this.schema = schema;
        this.value = InMemoryNumbers.valueOfSchema(schema);
        this.position = position;
    }

    public static Number of(String schema,Integer position) {
        return new Number(schema,position);
    }

    private boolean isValid(String value) {
        String[] splitValue = value.split(Constants.EOL);

        if (splitValue.length != ROW) return false;
        for (String it : splitValue) {
            if (it.length() != COL) return false;
        }

        return true;
    }

    public boolean isIllegible() {
        return Objects.equals(this.value, InMemoryNumbers.ERR);
    }

    public InMemoryNumbers getValue() {
        return value;
    }

    public String getSchema() {
        return schema;
    }

    public Integer getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return this.schema;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Number number = (Number) o;
        return value == number.value && Objects.equals(schema, number.schema);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, schema);
    }
}
