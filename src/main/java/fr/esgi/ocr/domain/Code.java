package fr.esgi.ocr.domain;

import fr.esgi.kernel.ValueObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class Code implements ValueObject {

    private final int id;
    private final List<Number> numbers;

    private Code(int id, List<Number> numbers) {
        this.id = id;
        this.numbers = numbers;
    }

    public static Code withoutNumbers(int id) {
        return new Code(id, new ArrayList<>());
    }

    public static Code of(int id, List<Number> numbers) {
        return new Code(id, numbers);
    }

    public int length() {
        return numbers.size();
    }

    public int col() {
        return numbers.size() * Number.COL;
    }

    public int row() {
        return Number.ROW;
    }

    public List<Number> getNumbers() {
        return numbers;
    }

    public String getValue() {
        StringBuilder result = new StringBuilder();
        this.numbers.forEach(number -> result.append(number.getValue().getLabel()));
        return result.toString();
    }

    public int getId() {
        return id;
    }

    public void addNumber(Number number) {
        this.numbers.add(number);
    }

    public boolean isIllegible() {
        return this.numbers.stream().anyMatch(Number::isIllegible);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        this.numbers.forEach(result::append);
        return result.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Code code = (Code) o;
        return Objects.equals(numbers, code.numbers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numbers);
    }
}
