package fr.esgi;

import fr.esgi.kernel.FileUtils;
import fr.esgi.ocr.application.*;
import fr.esgi.ocr.exposition.CliLauncher;
import fr.esgi.ocr.exposition.CliLauncherArgs;
import fr.esgi.ocr.exposition.CliLauncherInteractive;
import fr.esgi.ocr.exposition.CliLauncherRequest;

import java.io.IOException;
import java.util.Arrays;

public class ApplicationMain {

    public static void main(String[] args) throws IOException {
        CliLauncher cliLauncher;
        CliLauncherRequest cliLauncherRequest = new CliLauncherRequest();
        OCRCommandHandler ocrCommandHandler = new OCRCommandHandler(new ReadDirectoryFilesQueryHandler(), new RetrieveCodeValidationQueryHandler(), new ReadSchemasFromFileCommandHandler(), new ParseSchemasToCodesCommandHandler());

        if (args.length < 1) {
            cliLauncher = new CliLauncherInteractive(ocrCommandHandler);
        } else if (Arrays.asList(args).contains("/help")) {
            System.err.println(FileUtils.help());
            return;
        } else {
            cliLauncher = new CliLauncherArgs(ocrCommandHandler);
            cliLauncherRequest.setArgs(args);
        }

        cliLauncher.run(cliLauncherRequest);
    }

}
